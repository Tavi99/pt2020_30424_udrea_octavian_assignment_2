import java.io.FileWriter;
import java.util.ArrayList;

public class SimulationManager implements Runnable {

    private String inputPath;

    private String outPath;

    public void initializeAndStartServers(Server[] servers, SimulationData sd) {
        for(int index = 0; index < sd.getQ(); index++) {
            servers[index] = new Server(sd.getN());
            new Thread(servers[index]).start();
        }
    }

    public boolean checkServersBeingAllEmpty(Server[] servers, SimulationData sd) {

        for(int index = 0; index < sd.getQ(); index++)
            if(!servers[index].getClients().isEmpty()) {
                return false;
            }
        return true;
    }

    public void allowServersToProcessClients(Server[] servers, SimulationData sd) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int index = 0; index < sd.getQ(); index++)
            servers[index].setProcessClients(true);
    }

    public void denyServersToProcessClients(Server[] servers, SimulationData sd) {
        for(int index = 0; index < sd.getQ(); index++)
            servers[index].setProcessClients(false);
    }

    public static int getMinimumWaitingTimeServer(Server[] servers, SimulationData sd) {

        int minWaitingTime = sd.getN() * sd.gettMaxService();
        int serverIndex = 0;

        for(int index = 0; index < sd.getQ(); index++) {
            if(servers[index].getWaitingTime() < minWaitingTime) {
                minWaitingTime = servers[index].getWaitingTime();
                serverIndex = index;
            }
        }

        return serverIndex;
    }

    public double sendClientsToServers(Server[] servers, SimulationData sd, ArrayList<Client> clients, int tSimulation, double totalWaitingTime) {

        int serverIndexToAddClient = getMinimumWaitingTimeServer(servers, sd);
        if(!clients.isEmpty()) {
            while (!clients.isEmpty() && clients.get(0).gettArrival() == tSimulation) {
                servers[serverIndexToAddClient].addClient(clients.get(0));
                totalWaitingTime += servers[serverIndexToAddClient].getWaitingTime();
                clients.remove(0);
                serverIndexToAddClient = getMinimumWaitingTimeServer(servers, sd);
            }
        }

        return totalWaitingTime;
    }

    public void stopServers(Server[] servers, SimulationData sd) {
        for(int index = 0; index < sd.getQ(); index++) {
            servers[index].setServerOpen(false);
        }
    }

    public SimulationManager(String inputPath, String outPath) {
        this.inputPath = inputPath;
        this.outPath = outPath;
    }

    @Override
    public void run() {
        SimulationData sd = new SimulationData();
        FileWriter myWriter = FileProcessing.openFile(outPath);
        FileProcessing.readDataFromFile(sd, inputPath);

        ArrayList<Client> clients = new ArrayList<>();
        ClientGenerator.randomClientGenerator(clients, sd.getN(), sd.gettMinArrival(), sd.gettMaxArrival(), sd.gettMinService(), sd.gettMaxService());
        Server[] servers = new Server[sd.getQ()];
        initializeAndStartServers(servers, sd);

        int tSimulation = 0;
        double totalWaitingTime = 0;

        while(tSimulation <= sd.gettMaxSimulation()) {

            if(!checkServersBeingAllEmpty(servers, sd) || !clients.isEmpty()) {

                allowServersToProcessClients(servers, sd);
                totalWaitingTime = sendClientsToServers(servers, sd, clients, tSimulation, totalWaitingTime);
                FileProcessing.writeSimulationStatusIntoFile(myWriter, clients, servers, sd, tSimulation);
                tSimulation++;
                denyServersToProcessClients(servers, sd);
            }
            else {
                stopServers(servers, sd);
                tSimulation = sd.gettMaxSimulation() + 2;
            }
        }
        FileProcessing.writeAdditionalInfoIntoFile(myWriter, sd, totalWaitingTime);
        FileProcessing.closeFile(myWriter);
        System.exit(0);
    }

    public static void main(String[] args) {

        SimulationManager simulationManager = new SimulationManager(args[0], args[1]);
        Thread mainThread = new Thread(simulationManager);
        mainThread.start();
    }
}