import java.util.ArrayList;
import java.util.Collections;

public class ClientGenerator {

    public static void randomClientGenerator(ArrayList<Client> clients, int numberOfClients, int tMinArrival, int tMaxArrival, int tMinService, int tMaxService) {

        while(numberOfClients > 0) {

            int clientId = clients.size() + 1;
            int clientArrivalTime = (int)((Math.random() * ((tMaxArrival - tMinArrival) + 1)) + tMinArrival);
            int clientServiceTime = (int)((Math.random() * ((tMaxService - tMinService) + 1)) + tMinService);
            Client newClient = new Client(clientId, clientArrivalTime, clientServiceTime);
            clients.add(newClient);

            numberOfClients--;
        }

        Collections.sort(clients);
    }
}