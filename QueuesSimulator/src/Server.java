import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Server implements Runnable {

    private BlockingQueue<Client> clients;
    private int waitingTime;
    private boolean processClients;
    private boolean serverOpen;

    public boolean isServerOpen() {
        return serverOpen;
    }

    public void setServerOpen(boolean serverOpen) {
        this.serverOpen = serverOpen;
    }

    public boolean isProcessClients() {
        return processClients;
    }

    public void setProcessClients(boolean processClients) {
        this.processClients = processClients;
    }

    public BlockingQueue<Client> getClients() {
        return clients;
    }

    public void setClients(BlockingQueue<Client> clients) {
        this.clients = clients;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public Server(int capacity) {
        this.clients  = new ArrayBlockingQueue<Client>(capacity);
        this.waitingTime = 0;
        this.processClients = false;
        this.serverOpen = false;
    }

    public void addClient(Client newClient) {
        clients.add(newClient);
        setWaitingTime(getWaitingTime() + newClient.gettService());
        /**try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }

    public String toString() {

        if (clients.isEmpty())
            return "closed";
        else {
            String string = "";
            for(Client client : clients) {
                string += ("(" + client.getId() + "," + client.gettArrival() + "," + client.gettService() + ");");
            }
            return string;
        }
    }

    public void decreaseServiceTimeForClients() {

        if (!clients.isEmpty())
                if (clients.peek().gettService() > 0) {

                    clients.peek().settService(clients.peek().gettService() - 1);
                    setWaitingTime(getWaitingTime() - 1);

                    if (clients.peek().gettService() == 0)
                        clients.poll();
                }
    }

    @Override
    public void run() {

        setServerOpen(true);

        while(serverOpen) {

            if(clients.isEmpty()) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                if(isProcessClients()) {
                    decreaseServiceTimeForClients();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

}
