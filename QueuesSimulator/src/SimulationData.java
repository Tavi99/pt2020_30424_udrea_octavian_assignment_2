public class SimulationData {

    private int N;
    private int Q;
    private int tMaxSimulation;
    private int tMaxArrival;
    private int tMinArrival;
    private int tMaxService;
    private int tMinService;

    public int getN() {
        return N;
    }

    public void setN(int n) {
        N = n;
    }

    public int getQ() {
        return Q;
    }

    public void setQ(int q) {
        Q = q;
    }

    public int gettMaxSimulation() {
        return tMaxSimulation;
    }

    public void settMaxSimulation(int tMaxSimulation) {
        this.tMaxSimulation = tMaxSimulation;
    }

    public int gettMaxArrival() {
        return tMaxArrival;
    }

    public void settMaxArrival(int tMaxArrival) {
        this.tMaxArrival = tMaxArrival;
    }

    public int gettMinArrival() {
        return tMinArrival;
    }

    public void settMinArrival(int tMinArrival) {
        this.tMinArrival = tMinArrival;
    }

    public int gettMaxService() {
        return tMaxService;
    }

    public void settMaxService(int tMaxService) {
        this.tMaxService = tMaxService;
    }

    public int gettMinService() {
        return tMinService;
    }

    public void settMinService(int tMinService) {
        this.tMinService = tMinService;
    }
}
