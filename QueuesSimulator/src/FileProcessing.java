import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileProcessing {

    public static void readDataFromFile(SimulationData sd, String path) {
        try {
            FileInputStream file = new FileInputStream(path);
            Scanner scanner = new Scanner(file);
            int numberOfLines = 1;
            while(scanner.hasNextLine() && numberOfLines <= 5) {
                switch(numberOfLines) {
                    case 1:
                        sd.setN(Integer.parseInt(scanner.nextLine()));
                        break;
                    case 2:
                        sd.setQ(Integer.parseInt(scanner.nextLine()));
                        break;
                    case 3:
                        sd.settMaxSimulation(Integer.parseInt(scanner.nextLine()));
                        break;
                    case 4:
                        String line = scanner.nextLine();
                        String[] values = line.split(",");
                        sd.settMinArrival(Integer.parseInt(values[0]));
                        sd.settMaxArrival(Integer.parseInt(values[1]));
                        break;
                    case 5:
                        String line1 = scanner.nextLine();
                        String[] values1 = line1.split(",");
                        sd.settMinService(Integer.parseInt(values1[0]));
                        sd.settMaxService(Integer.parseInt(values1[1]));
                        break;
                    default:
                        break;
                }
                numberOfLines++;
            }
            scanner.close();
        } catch(IOException e) {
            System.out.println("An error occured!");
        }
    }

    public static FileWriter openFile(String path) {

        FileWriter myWriter = null;
        try {
            myWriter = new FileWriter(path);
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }

        return myWriter;
    }

    public static void writeSimulationStatusIntoFile(FileWriter myWriter, ArrayList<Client> clients, Server[] servers, SimulationData sd, int tSimulation) {

        try {
            myWriter.write("Time " + tSimulation);
            myWriter.write("\nWaiting clients: ");

            for(Client client : clients) {
                myWriter.write("(" + client.getId() + "," + client.gettArrival() + "," + client.gettService() + ");");
            }
            myWriter.write("\n");

            for(int index = 0; index < sd.getQ(); index++) {
                myWriter.write("Queue " + (index + 1) + ": " + servers[index].toString() + "\n");
            }
            myWriter.write("\n");
        } catch (IOException e) {
            try {
                myWriter.write("An error occurred.");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void writeAdditionalInfoIntoFile(FileWriter myWriter, SimulationData sd, double totalWaitingTime) {
        try {
            myWriter.write("\nAverage waiting time: " + (totalWaitingTime / sd.getN()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void closeFile(FileWriter myWriter) {
        try {
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }
}
